#! /usr/bin/env python3

import os
import PyQt5.uic
import PyInstaller.__main__ as PyInstaller

directory = os.path.dirname(os.path.realpath(__file__))
spec_file = os.path.join(directory, 'pygu-patcher.spec')

PyQt5.uic.compileUiDir(directory, True)
PyInstaller.run(['pygu-patcher.spec', '-y'])