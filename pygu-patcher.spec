# -*- mode: python -*-
import sys

a = Analysis(["src/main.py"],
             pathex=["src"],
             hiddenimports=[],
             hookspath=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          # Static link the Visual C++ Redistributable DLLs if on Windows
          a.binaries + [("msvcp100.dll", "C:\\Windows\\System32\\msvcp100.dll", "BINARY"),
                        ("msvcr100.dll", "C:\\Windows\\System32\\msvcr100.dll", "BINARY")]
          if sys.platform.startswith("win32") else a.binaries,
          a.zipfiles,
          a.datas + [("images/patcher.ico", "src/images/patcher.ico", "DATA")],
          name=os.path.join("dist", ("PYGU Patcher.exe" if sys.platform == "win32" else "pygu-patcher")),
          debug=False,
          strip=None,
          upx=False,
          console=False,
          icon="src/images/patcher.ico")

if sys.platform == "darwin":
    app = BUNDLE(exe,
                 name="PYGU Patcher.app",
                 icon="src/images/patcher.icns")
