#!/usr/bin/env python3

import hashlib
import json
import logging
import os
import pathlib
import re
import requests
import shutil
import subprocess
import sys
import time
import vdf
import webbrowser

from CaseInsensitiveDict import CaseInsensitiveDict as cidict
from PyQt5 import QtWidgets, QtCore, QtGui
from qt.MainWindow import Ui_MainWindow
from qt.Settings import Ui_SettingsDialog
from qt.Changelog import Ui_ChangelogDialog


class DownloadThread(QtCore.QThread):
    progress = QtCore.pyqtSignal(int)
    status = QtCore.pyqtSignal("QString")

    def setup(self, cache, url, checksum):
        self.cache = cache
        self.checksum = checksum
        self.url = url

    def run(self):
        skip = False

        if os.path.isfile(self.cache):
            self.status.emit("checking cached file...")
            self.progress.emit(0)
            if md5(self.cache) == self.checksum:
                skip = True

        if not skip:
            r = requests.get(self.url, stream=True)
            # get total size in bytes from headers
            t = int(r.headers["Content-Length"].strip())
            # downloaded bytes
            d = 0

            with open(self.cache, "wb") as f:
                for chunk in r.iter_content(chunk_size=1024):
                    f.write(chunk)
                    d += 1024
                    p = int(d * 100 / t)

                    self.progress.emit(p)
                    self.status.emit("Downloaded {0:.1f}/{1:.1f} MB"
                                     .format(d / 1000000, t / 1000000))


class InstallThread(QtCore.QThread):
    progress = QtCore.pyqtSignal(int)
    status = QtCore.pyqtSignal("QString")

    def setup(self, cache_dir, game_dir, appid,
              backup, depots=None, fixfiles=None):
        self.appid = appid
        self.backup = backup
        self.depots = depots
        self.fixfiles = fixfiles
        self.game_dir = game_dir
        self.cache_dir = cache_dir

    def run(self):
        cache_dir = self.cache_dir
        patch_file = os.path.join(cache_dir, "download",
                                  "{}.tar.gz".format(self.appid))
        backup_dir = os.path.join(cache_dir, "backup", str(self.appid))
        backup_root = os.path.join(cache_dir, "backup")
        extract_dir = os.path.join(cache_dir, "temp", str(self.appid))
        uninstall = []

        if self.backup and not os.path.isdir(backup_dir):
            os.makedirs(backup_dir)

        cfile = 0
        fsum = 0

        self.progress.emit(0)
        self.status.emit("unpacking archive...")
        shutil.unpack_archive(patch_file, extract_dir, "gztar")

        self.status.emit("patching game files...")
        # count files to be patched
        for root, dirs, files in os.walk(extract_dir):
            fsum += len(files)

        for root, dirs, files in os.walk(extract_dir):
            for name in files:
                cfile += 1
                relroot = os.path.relpath(root, extract_dir)

                self.progress.emit(cfile / fsum * 100)

                # create the parent directory if it doesn't exist
                gl_root = os.path.join(self.game_dir, relroot)
                if not os.path.isdir(gl_root):
                    os.makedirs(gl_root)

                if self.backup:
                    bu_root = os.path.join(backup_dir, relroot)
                    if os.path.isfile(os.path.join(gl_root, name)):
                        if not os.path.isdir(bu_root):
                            os.makedirs(bu_root)
                        shutil.move(os.path.join(gl_root, name),
                                    os.path.join(bu_root, name))
                    uninstall.append(os.path.join(relroot, name))

                shutil.copy(os.path.join(root, name),
                            os.path.join(gl_root, name))

        if self.backup:
            jsonf = os.path.join(backup_root,
                                 "uninstall_{}.json".format(self.appid))
            json.dump(uninstall, open(jsonf, "w"))

        if self.depots:
            fname = "appmanifest_{0}.acf".format(self.appid)
            fpath = os.path.join(uppath(self.game_dir, 2), fname)
            appmanifest = vdf.load(open(fpath))

            if self.backup:
                    shutil.copy(fpath, backup_root)

            for key in self.depots["del"]:
                appmanifest["AppState"]["MountedDepots"].pop(str(key), None)

            for key, val in self.depots["add"].items():
                appmanifest["AppState"]["MountedDepots"][key] = val

            vdf.dump(appmanifest, open(fpath, 'w'), pretty=True)

        if self.fixfiles:
            if sys.platform.startswith("win32"):
                self.fixfiles = os.path.normpath(self.fixfiles)
                self.fixfiles = self.fixfiles.replace("\\", "\\\\")

            for root, dirs, files in os.walk(self.game_dir, topdown=True):
                for name in files:
                    relpath = os.path.join(
                                os.path.relpath(root, self.game_dir),
                                name)

                    if re.match(self.fixfiles, relpath):
                        if self.backup:
                            bu_root = os.path.join(backup_dir, relpath)
                            if os.path.isfile(os.path.join(gl_root, name)):
                                shutil.move(os.path.join(gl_root, relpath),
                                            os.path.join(bu_root, relpath))
                            uninstall.append(os.path.join(relpath, name))

                        else:
                            os.remove(os.path.join(root, name))

        if os.path.isdir(os.path.join(cache_dir, "temp")):
            shutil.rmtree(os.path.join(cache_dir, "temp"))


def cache_clean():
    try:
        shutil.rmtree(os.path.join(cache, "patches", "download"))
    except FileNotFoundError as e:
        pass
    except Exception as e:
        logging.warning(e)


def cache_dir(debug=False):
    if debug:
        cache = os.path.join(uppath(os.path.abspath(__file__), 2),
                             "test", "cache")

    elif sys.platform.startswith("win32"):
        cache = os.path.join(os.environ["LOCALAPPDATA"], "pygu-patcher")

    elif sys.platform.startswith("linux"):
        cache = os.path.join(
            os.environ["HOME"], ".cache", "pygu-patcher")

    elif sys.platform.startswith("darwin"):
        cache = os.path.join(
            os.environ["HOME"], "Library", "Caches", "pygu-patcher")

    if not os.path.isdir(cache):
        os.mkdir(cache)

    return cache


def cache_open():
    try:
        if sys.platform.startswith("win32"):
            os.startfile(cache)
        elif sys.platform.startswith("linux"):
            subprocess.Popen(["xdg-open", cache])
        elif sys.platform.startswith("darwin"):
            subprocess.call(['open', cache])
        return True
    except:
        logging.warning("could not open the file browser.")
        return False


def install_patch(appid=None, backup=False):
    # get appid from table if it was not given to function
    if not appid:
        appid = select_game()

    ui_Main.pbDownload.setEnabled(False)

    # get info from the data object
    for game in data["games"]:
        if game["appid"] == appid:
            dlfile = mirror + game["patches"][0]["file"]
            checksum = game["patches"][0]["checksum"]
            try:
                ffixes = game["patches"][0]["ffix"]
            except:
                ffixes = None
            try:
                depots = game["patches"][0]["depots"]
            except:
                depots = None

    # get info from appmanifest
    for game in scan_appmanifests(steamapps):
        if game[0] == str(appid):
            gamename = game[1]
            gamedir = game[2]

    os.makedirs(os.path.join(cache, "patches", "download"), exist_ok=True)

    ui_Main.gbDownload.show()
    cachefile = os.path.join(cache, "patches", "download",
                             "{}.tar.gz".format(appid))

    if sys.platform.startswith("win32"):
        ui_Main.tprogress.setVisible(True)

    thread_dl.setup(cachefile, dlfile, checksum)
    thread_dl.start()
    thread_dl.finished.connect(thread_install.start)

    thread_install.setup(os.path.join(cache, "patches"), gamedir, appid,
                         backup, depots=depots, fixfiles=ffixes)

    thread_install.finished.connect(lambda: install_finished(gamename))


def install_finished(game_name):
    ui_Main.statusBar.showMessage("{} has been patched.".format(game_name))
    ui_Main.pbDownload.setEnabled(True)


def md5(file_name):
    hash_md5 = hashlib.md5()
    with open(file_name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def uppath(path, n=1):
    return os.sep.join(path.split(os.sep)[:-n])


def find_steamapps():
    if sys.platform.startswith("win32"):
        import winreg
        path64 = r"SOFTWARE\WOW6432Node\Valve\Steam"
        path32 = r"SOFTWARE\Valve\Steam"
        Reg = winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE)

        try:
            try:
                Key = winreg.OpenKey(Reg, path64)
            except:
                Key = winreg.OpenKey(Reg, path32)

            for i in range(256):
                try:
                    name, val, typ = winreg.EnumValue(Key, i)
                    if name.lower() == "installpath":
                        steampath = os.path.join(val, "steamapps")
                except:
                    break
        except:
            return False

    elif sys.platform.startswith("linux"):
        steampath = os.path.join(
                os.environ["HOME"], ".steam", "steam", "steamapps")

    elif sys.platform.startswith("darwin"):
        steampath = os.path.join(
                os.environ["HOME"], "Library", "Application Support", "Steam",
                "steamapps")

    if os.path.isdir(steampath):
        return steampath
    else:
        return False


def get_res_path(relative_path):
    # try to get from pyinstaller temp dir
    try:
        path = os.path.join(sys._MEIPASS, relative_path)

    # else from relative dir
    except:
        path = os.path.join(uppath(os.path.abspath(__file__)), relative_path)

    if os.path.exists(path):
        return path
    else:
        return None


def get_patch_info(cache, url="default"):
    if url == "default":
        url = "https://raw.githubusercontent.com/PYGU/mirrors/master/info.json"
    fname = os.path.join(cache, "info.json")
    lastmod = 0

    if os.path.isfile(fname):
        lastmod = time.time() - os.stat(fname).st_mtime

    if lastmod > 1800 or not os.path.isfile(fname):
        try:
            r = requests.get(url)
            json.dump(r.json(), open(fname, "w", encoding="utf-8"))
            return r.json()
        except:
            logging.info("could not get patch data from github.")
            return False
    else:
        return json.load(open(fname, encoding="utf-8"))


def check_for_update(current_ver):
    url = "https://api.github.com/repos/PYGU/patcher/releases/latest"

    fname = os.path.join(cache, "lastupdatecheck")
    dtime = 0
    if os.path.isfile(fname):
        dtime = time.time() - os.stat(fname).st_mtime

    if dtime > 1800 or not os.path.isfile(fname):
        try:
            r = requests.get(url)
            updateinfo = r.json()

            pathlib.Path(fname).touch()

            if updateinfo["tag_name"] == current_ver:
                return False
            else:
                ui_Changelog.info.setPlainText(updateinfo["body"])
                ui_Changelog.headline.setText(updateinfo["name"])
                ui_Changelog.pbDownload.clicked.connect(
                    lambda: webbrowser.open(updateinfo["html_url"]))
                Changelog.show()
                return True
        except:
            logging.info("could not get update data from github.")
            return False


def get_settings():
    if os.path.isfile(settings_file):
        settings = json.load(open(settings_file))
    else:
        settings = {"backup": False, "mirror": 0}

    ui_Settings.cbInstallBackup.setCheckState(settings["backup"])
    ui_Settings.qbMirror.setCurrentIndex(settings["mirror"])
    return settings


def set_settings():
    backup = ui_Settings.cbInstallBackup.checkState()
    mirror = ui_Settings.qbMirror.currentIndex()
    json.dump({"backup": backup, "mirror": mirror}, open(settings_file, "w"))


def select_game():
    ui_Main.pbDownload.setEnabled(True)
    sel_row = ui_Main.table.selectedItems()[0].row()
    sel_header = ui_Main.table.verticalHeaderItem(sel_row)
    appid = int(sel_header.text())
    for game in data["games"]:
        if game["appid"] == appid:
            ui_Main.tbPatchInfo.setPlainText(game["patches"][0]["info"]["de"])

    return appid


def scan_appmanifests(steamapps):
    # TODO: add argument to scan one appid explicitly
    detected_games = []
    manifest_filter = []
    steamapps = [steamapps]

    for game in data["games"]:
        manifest_filter.append("appmanifest_{}.acf".format(game["appid"]))

    handle = open(os.path.join(steamapps[0], "libraryfolders.vdf"))
    folders = vdf.load(handle)

    for i in range(1, 16):
        try:
            f = folders["LibraryFolders"][str(i)]
            steamapps.append(os.path.join(f, "steamapps"))
        except:
            break

    for appfolder in steamapps:
        for entry in os.listdir(appfolder):
            entryf = os.path.join(appfolder, entry)

            if os.path.isfile(entryf) and entry in manifest_filter:
                manifest = cidict(vdf.load(open(entryf, encoding="utf-8")))
                app_id = manifest["AppState"]["appid"]
                app_name = manifest["AppState"]["name"]
                installdir = os.path.join(appfolder, "common",
                                          manifest["AppState"]["installdir"])

                detected_games.append([app_id, app_name,
                                       installdir, entryf])
    return detected_games


if __name__ == "__main__":
    # cache has to be defined first
    cache = cache_dir()

    # logging configuration
    logging.basicConfig(filename=os.path.join(cache, "patcher.log"),
                        format="[%(asctime)s] %(levelname)s - %(message)s",
                        datefmt="%d.%m.%Y %H:%M:%S",
                        level=logging.DEBUG)

    console = logging.StreamHandler()
    logging.getLogger("").addHandler(console)
    # Qt config
    App = QtWidgets.QApplication(sys.argv)
    Main = QtWidgets.QMainWindow()
    Settings = QtWidgets.QWidget()
    Changelog = QtWidgets.QWidget()
    Main.setWindowIcon(QtGui.QIcon(get_res_path("images/patcher.ico")))
    Settings.setWindowIcon(QtGui.QIcon(get_res_path("images/patcher.ico")))
    Changelog.setWindowIcon(QtGui.QIcon(get_res_path("images/patcher.ico")))

    ui_Main = Ui_MainWindow()
    ui_Main.setupUi(Main)

    ui_Settings = Ui_SettingsDialog()
    ui_Settings.setupUi(Settings)

    ui_Changelog = Ui_ChangelogDialog()
    ui_Changelog.setupUi(Changelog)

    # define globals
    data = get_patch_info(cache)
    steamapps = find_steamapps()
    settings_file = os.path.join(cache, "settings.json")
    settings = get_settings()
    PATCHER_APPVER = "v0.2"

    # set up threads
    thread_dl = DownloadThread()
    thread_dl.progress.connect(ui_Main.progressBar.setValue)
    thread_dl.status.connect(ui_Main.statusBar.showMessage)

    thread_install = InstallThread()
    thread_install.progress.connect(ui_Main.progressBar.setValue)
    thread_install.status.connect(ui_Main.statusBar.showMessage)

    # set up main window
    urls = {"BugReport": "https://github.com/PYGU/patcher/issues",
            "Contribute": "https://github.com/PYGU/patcher",
            "Support": "https://steamcommunity.com/groups/PYGU/discussions/1"}

    ui_Main.actionAboutQt.triggered.connect(
            lambda: QtWidgets.QMessageBox.aboutQt(Main))
    ui_Main.actionBugReport.triggered.connect(
            lambda: webbrowser.open(urls["BugReport"]))
    ui_Main.actionContribute.triggered.connect(
            lambda: webbrowser.open(urls["Contribute"]))
    ui_Main.actionSupport.triggered.connect(
            lambda: webbrowser.open(urls["Support"]))

    ui_Main.actionPreferences.triggered.connect(Settings.show)
    ui_Main.gbDownload.hide()
    ui_Main.pbDownload.clicked.connect(
            lambda: install_patch(backup=settings["backup"]))
    ui_Main.table.itemSelectionChanged.connect(select_game)

    for game in scan_appmanifests(steamapps):
        rowcount = ui_Main.table.rowCount()
        ui_Main.table.setRowCount(rowcount + 1)
        item = QtWidgets.QTableWidgetItem()
        item.setText(game[0])
        ui_Main.table.setVerticalHeaderItem(rowcount, item)
        ui_Main.table.setItem(rowcount, 0, QtWidgets.QTableWidgetItem())
        ui_Main.table.setItem(rowcount, 1, QtWidgets.QTableWidgetItem())
        ui_Main.table.item(rowcount, 0).setText(game[1])
        ui_Main.table.item(rowcount, 1).setText(game[2])

    # set up settings dialog
    ui_Settings.qbMirror.addItems(data["mirrors"])
    mirror = data["mirrors"][settings["mirror"]]
    ui_Settings.pbCacheClean.clicked.connect(cache_clean)
    ui_Settings.pbCacheOpen.clicked.connect(cache_open)
    ui_Settings.cbInstallBackup.stateChanged.connect(set_settings)
    ui_Settings.qbMirror.currentIndexChanged.connect(set_settings)

    Main.show()

    # Windows-specific main window functionality
    if sys.platform.startswith("win32"):
        from PyQt5 import QtWinExtras
        ui_Main.tbutton = QtWinExtras.QWinTaskbarButton(Main)
        ui_Main.tbutton.setWindow(Main.windowHandle())
        ui_Main.tprogress = ui_Main.tbutton.progress()
        thread_dl.progress.connect(ui_Main.tprogress.setValue)
        thread_install.progress.connect(ui_Main.tprogress.setValue)
        thread_install.finished.connect(
                lambda: ui_Main.tprogress.setVisible(False))

    # check for update
    check_for_update(PATCHER_APPVER)

    sys.exit(App.exec_())
