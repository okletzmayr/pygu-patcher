#!/usr/bin/env python3

from main import cache_dir, DownloadThread, InstallThread, md5, get_patch_info
import os
import pathlib
import shutil
import sys
import unittest
import vdf


def create_steamapps(sdir, gdir):
    os.makedirs(gdir, exist_ok=True)

    manifest = {
        'AppState':
            {
                'appid': '1337',
                'Universe': '1',
                'name': 'PYGU Test',
                'installdir': 'PYGU Test',
                'MountedDepots':
                {
                    '1338': '0123456789123456789'
                },
                'StateFlags': '4'
                }
        }

    # create libraryfolders.vdf
    p = pathlib.Path(os.path.join(sdir, "libraryfolders.vdf"))
    p.touch()

    # create appmanifest_1337.acf
    vdf.dump(manifest, open(os.path.join(sdir, "appmanifest_1337.acf"), "w"))

    # create ./executable
    p = pathlib.Path(os.path.join(gdir, "Executable.exe"))
    p.touch()

    # create ./folder
    os.mkdir(os.path.join(gdir, "folder"))

    # create ./folder/{file0..file9}
    for i in range(10):
        p = pathlib.Path(os.path.join(gdir, "folder", "file" + str(i)))
        p.touch()

    # create ./folder/{oldfile0..oldfile4}
    for i in range(5):
        p = pathlib.Path(os.path.join(gdir, "folder", "oldfile" + str(i)))
        p.touch()


def uppath(path, n):
    return os.sep.join(path.split(os.sep)[:-n])


class TestMain(unittest.TestCase):
    dlfile = os.path.join(cache_dir(True), "test.tar.gz")

    def test_get_patch_info(self):
        mock_dict = {
                        "mirrors": ["https://pb115.com/pygu/"],
                        "games": [{
                            "appid": 1337,
                            "patches": [{
                                "file": "test.tar.gz",
                                "checksum": "599be8795be229c3d45bb871b2edca9d",
                                "depots": {
                                    "add": {
                                        "1339": "9876543219876543210"
                                    },
                                    "del": [1338]
                                },
                                "ffix": "oldfile[0-4]",
                                "info": {
                                    "de": "Test-App"
                                }
                            }]
                        }]
                    }
        mock_url = "https://raw.githubusercontent.com/PYGU/mirrors/test/info.json"
        self.assertDictEqual(get_patch_info(dbg_cache, url=mock_url), mock_dict)
        self.assertFalse(get_patch_info(dbg_cache, url=""))

    def test_cache_dir(self):
        self.assertEqual(cache_dir(True), dbg_cache)

        if sys.platform.startswith("win32"):
            self.assertIsNotNone(cache_dir())
        elif sys.platform.startswith("linux"):
            self.assertIsNotNone(cache_dir())

    def test_download_thread(self):
        thread_dl = DownloadThread()
        thread_dl.setup(self.dlfile, "https://pb115.com/pygu/test.tar.gz")
        thread_dl.run()

        self.assertTrue(os.path.isfile(self.dlfile))

    # def test_install_thread(self):
    #     thread_install = InstallThread()
    #     thread_install.setup(self.dlfile, dbg_gamedir, 42700, debug=True,
    #                          depots={"add": {"1339": "9876543219876543210"},
    #                                  "del": [1338]},
    #                          filefixes="oldfile[0-4]")

    def test_md5(self):
        self.assertEqual(md5(self.dlfile), "599be8795be229c3d45bb871b2edca9d")

if __name__ == "__main__":
    # relative root of __file__
    rr = uppath(os.path.abspath(__file__), 2)

    # debug dirs
    dbg_cache = os.path.join(rr, "test", "cache")
    dbg_steam = os.path.join(rr, "test", "steamapps")
    dbg_gamedir = os.path.join(dbg_steam, "common", "PYGU Patcher")

    # clean cache before testing
    if os.path.isdir(dbg_cache):
        shutil.rmtree(dbg_cache)

    # clean and re-create steamapps
    if os.path.isdir(dbg_steam):
        shutil.rmtree(dbg_steam)
    create_steamapps(dbg_steam, dbg_gamedir)

    unittest.main(verbosity=1)
