PYGU-Patcher: A Manager for Uncut-Patches
=========================================

[![win build status](https://img.shields.io/appveyor/ci/okletzmayr/patcher.svg?maxAge=21600)][appveyor]
[![unix build status](https://img.shields.io/travis/PYGU/patcher.svg?maxAge=21600)][travis]
[![license](https://img.shields.io/github/license/PYGU/patcher.svg?maxAge=2592000)][gpl]
[![GitHub downloads](https://img.shields.io/github/downloads/PYGU/patcher/total.svg?maxAge=86400)][releases]


* [Intro](#intro)
* [Feature Summary](#feature-summary)
* [Contributing](#contributing)
  * [Bug reports and feature requests](#bug-reports-and-feature-requests)
  * [Mirror hosting](#mirror-hosting)
* [Building from source](#building-from-source)
* [Contact](#contact)
* [License](#license)


Intro
-----

[Patcher] is a manager for uncut-patches.  [PYGU] is a Steam group dedicated to
offer you the experience the game developers had in mind, regardless of what
some [government institutions][BPjM] deem as "harmful".

If you have not heard of anything like this before, and would like to learn
more, [forUncut!][forUncut] has a great [guide] on game censorship in Germany.


Feature Summary
---------------

* Automatically detects your Steam path, and paths of installed games
* Checks your installed Games for our latest Uncut-Patches
* Easy automated download & install process


Contributing
------------

**Important Note**:
Even though [Patcher] will mostly be used in Germany, please keep development
discussion in English, as we aim for internationalization with this app. You
can report bugs in German on our [Steam forum][ger-forum].

[Patcher] has been written in Python to allow as many people as possible to
read and improve our source code. Check the [issues] for currently unassigned
tasks.

In the coming weeks, contributing rules will follow. For now, just send a Pull
Request and we'll discuss it.

### Bug reports and feature requests

[Patcher] is in active development. If you encounter bugs or have ideas you'd
like to see implemented, please open an [issue][issues].

### Mirror hosting

We're currently hosting all patches ourselves, but we're looking to expand. If
you're running a server and have disk space and traffic to spare, and would
like to help us, check [PYGU/mirrors][mirrors]. Additional mirrors would be
appreciated.


Building from Source
--------------------

**Note**: Because of problems with PyInstaller, only Python 3.5.x is supported.
          [pyenv] is recommended for development on UNIX systems.


Make sure `git` and `python` are available in `PATH`, then run:

```
git clone https://github.com/PYGU/patcher.git pygu-patcher
cd pygu-patcher
python make.py
```


Contact
-------

[Patcher] is currently maintained by [Oliver Kletzmayr][@okletzmayr].

Questions regarding patches should be directed to our [Steam Group][PYGU].


License
-------

This software is licensed under the [GPL v3 license][gpl].



[Patcher]: https://github.com/PYGU/patcher
[PYGU]: https://steamcommunity.com/groups/PYGU

[appveyor]: https://ci.appveyor.com/project/okletzmayr/patcher
[BPjM]: https://en.wikipedia.org/wiki/BPjM
[forUncut]: https://steamcommunity.com/groups/forUncut
[ger-forum]: http://steamcommunity.com/groups/PYGU/discussions/1/359547436758008138/
[gpl]: LICENSE.md
[guide]: https://steamcommunity.com/sharedfiles/filedetails/?id=703679729
[issues]: https://github.com/PYGU/patcher/issues
[mirrors]: https://github.com/PYGU/mirrors
[pyenv]: https://github.com/pyenv/pyenv
[releases]: https://github.com/PYGU/patcher/releases
[travis]: https://travis-ci.org/PYGU/patcher

[@okletzmayr]: https://github.com/okletzmayr
